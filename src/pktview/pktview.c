/*
        SoupGate Open-Source License

        Copyright (c) 1999-2000 by Tom Torfs
        
        The SoupGate software and its documentation may freely be distributed
        and used for all purposes, provided no fee is charged other than to
        cover administration and distribution costs, in other words it may
        not be sold for profit.

        The SoupGate software may freely be modified. Source code need
        not be made available for modified versions or derived programs,
        but if it is not, at least a copy of this license must be included
        in the program or its documentation.

        Modified source code may be made available, provided this license
        remains included, intact and unmodified, and the fact that changes
        were made must clearly be identified in both the source code and
        documentation, and a reference must be provided as to where the
        original, unmodified version can be obtained.

        DISCLAIMER:  THE AUTHOR EXCLUDES ANY AND ALL IMPLIED
        WARRANTIES,  INCLUDING WARRANTIES OF MERCHANTABILITY
        AND FITNESS FOR A PARTICULAR PURPOSE.     THE AUTHOR
        MAKES NO WARRANTY OR REPRESENTATION,  EITHER EXPRESS
        OR IMPLIED,   WITH RESPECT TO THIS SOFTWARE,     ITS
        QUALITY,  PERFORMANCE,  MERCHANTABILITY,  OR FITNESS
        FOR A PARTICULAR PURPOSE.   THE AUTHOR SHALL HAVE NO
        LIABILITY FOR SPECIAL,  INCIDENTAL, OR CONSEQUENTIAL
        DAMAGES ARISING OUT OF  OR RESULTING FROM THE USE OR
        MODIFICATION OF THIS SOFTWARE.

	End of License
*/

/* PKTView - Fidonet *.PKT viewer */

/* link with wildargv.c (or your compiler's equivalent) */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*...sPKT structure:0:*/
const char *attributes[16]
   = {"Pvt","Cra","Rec","Snt","F/A","I/T","Orp","K/S",
      "Loc","Hld","(?)","Frq","Rrq","Cpt","Aud","Urq"};

/* disable structure alignment */
#if defined(__GNUC__)
   #define PACKED __attribute__((packed))
#else
   #define PACKED
   #if defined(_MSC_VER) || defined(_QC) || defined(__WATCOMC__)
      #pragma pack(1)
   #elif defined(__ZTC__)
      #pragma ZTC align 1
   #elif defined(__TURBOC__) && (__TURBOC__ > 0x202)
      #pragma option -a-
   #endif
#endif

/* FSC-39 (Type 2+) packet definition */

struct PACKETHEADER {
   unsigned short orignode,destnode,
                  year,month,day,
                  hour,minute,second,
                  baudrate,
                  version,
                  orignet,destnet;
   unsigned char  pcodelow,prevmajor,
                  password[8];
   unsigned short qmorigzone,qmdestzone,
                  auxnet,cwvalidate;
   unsigned char  pcodehigh,prevminor;
   unsigned short capword,
                  origzone,destzone,
                  origpoint,destpoint;
   unsigned long  extrainfo;
  } PACKED;

struct PACKETMSGHEADER {
   unsigned short version,
                  orignode,destnode,
                  orignet,destnet,
                  attrib,
                  cost;
  } PACKED;
/*...e*/

/*...szgetc:0:*/
/* read a character from a file, and return '\0' on EOF */
char zgetc(FILE *fp)
{
   int ch=getc(fp);
   if (ch==EOF)
      ch = '\0';
   return (char)ch;
}
/*...e*/
/*...sreadasciiz:0:*/
/* read a nul-terminated string from a file */
size_t readasciiz(FILE *fp, char *s, size_t maxlen)
{
   size_t count;
   for (count=0; count<maxlen && (s[count]=zgetc(fp))!='\0'; count++) ;
   return count;
}
/*...e*/
/*...sreadasciicr:0:*/
/* read a nul or CR-terminated string from a file
   returns 0 if end of ASCIIZ string reached,
   1 if \r reached
   2 if maxlen reached) */
int readasciicr(FILE *fp, char *s, size_t maxlen)
{
   size_t count;
   for (count=0; count<maxlen; count++)
   {
      s[count] = zgetc(fp);
      if (s[count]=='\r')
      {
         s[count] = '\0';
         return 1;
      }
      else if (s[count]=='\0')
      {
         return 0;
      }
   }

   s[count] = '\0';
   return 2;
}
/*...e*/

/*...sdisplayline:0:*/
void displayline(char *s, int beg)
{
   int l,c;
   l = strlen(s);
   for (c=0; c<l; c+=79-beg)
   {
      printf("%.*s\n",79-beg,s+c);
      beg = 0;
   }
}
/*...e*/

/*...spktview:0:*/
int pktview(char *name)
{
   const char *error = NULL;
   FILE *fpkt = NULL;
   int type2plus;
   long msgcount;
   int i;
   static struct PACKETHEADER pktheader;
   static struct PACKETMSGHEADER pktmsgheader;
   static char msgdate[21];
   static char msgto[128];
   static char msgfrom[128];
   static char msgsubj[128];
   static char linebuf[1025];
   long fofs;
   int status;

   printf("%s:\n",name);

   fpkt = fopen(name,"rb");
   if (fpkt==NULL)
   {
      error = "file can't be opened";
      goto abort;
   }

   if (fread(&pktheader,1,sizeof pktheader,fpkt) < sizeof pktheader)
   {
      error = "file is not a valid PKT file";
      goto abort;
   }

   if (pktheader.version!=2)
   {
      error = "file is not a Type 2 or Type 2+ PKT file";
      goto abort;
   }

   if (pktheader.capword!=0x0001 || pktheader.cwvalidate!=0x0100)
      type2plus = 0;
   else
      type2plus = 1;

   printf("Detected Type 2%s packet\n", type2plus?"+":"");

   printf("--- PACKET HEADER ---\n");
   printf("Packet version        : %u\n",pktheader.version);
   printf("Capability word       : %04X\n",pktheader.capword);
   printf("Capability validation : %04X\n",pktheader.cwvalidate);
   printf("Product code          : %u.%u\n",pktheader.pcodehigh,pktheader.pcodelow);
   printf("Product revision      : %u.%u\n",pktheader.prevmajor,pktheader.prevminor);
   printf("Product specific info : %08lX\n",pktheader.extrainfo);
   printf("Date and time         : %04u-%02u-%02u %02u:%02u:%02u\n",
          pktheader.year, pktheader.month+1, pktheader.day,
          pktheader.hour, pktheader.minute, pktheader.second);
   printf("Originating zone      : ");
   if (type2plus)
      printf("%u\n", pktheader.origzone);
   else
      printf("(%u)\n", pktheader.qmorigzone);
   printf("Originating net       : %u\n",pktheader.orignet);
   printf("Originating node      : %u\n",pktheader.orignode);      
   printf("Originating point     : ");
   if (type2plus)
      printf("%u\n", pktheader.origpoint);
   else
      printf("(n/a)\n");
   printf("Destination zone      : ");
   if (type2plus)
      printf("%u\n", pktheader.destzone);
   else
      printf("(%u)\n", pktheader.qmdestzone);
   printf("Destination net       : %u\n",pktheader.destnet);
   printf("Destination node      : %u\n",pktheader.destnode);      
   printf("Destination point     : ");
   if (type2plus)
      printf("%u\n", pktheader.destpoint);
   else
      printf("(n/a)\n");
   printf("Auxiliary net         : %u\n",pktheader.auxnet);
   printf("Baudrate              : %u\n",pktheader.baudrate);
   printf("Password              : %.8s\n",pktheader.password);

   msgcount = 0;
   while (fread(&pktmsgheader,1,sizeof pktmsgheader,fpkt)==sizeof pktmsgheader)
   {
      msgcount++;
      printf("--- PACKED MESSAGE #%ld ---\n", msgcount);
      readasciiz(fpkt,msgdate,20);
      readasciiz(fpkt,msgto,127);
      readasciiz(fpkt,msgfrom,127);
      readasciiz(fpkt,msgsubj,127);
      printf("Version            : %u\n",pktmsgheader.version);
      printf("Originating net    : %u\n",pktmsgheader.orignet);
      printf("Originating node   : %u\n",pktmsgheader.orignode);
      printf("Destination net    : %u\n",pktmsgheader.destnet);
      printf("Destination node   : %u\n",pktmsgheader.destnode);
      printf("Cost               : %u\n",pktmsgheader.cost);
      printf("Attributes         :");
      for (i=0; i<16; i++)
         if ((pktmsgheader.attrib>>i)&1)
            printf(" %s", attributes[i]);
      printf("\n");
      printf("Date and time      : %s\n",msgdate);
      printf("From               : %s\n",msgfrom);
      printf("To                 : %s\n",msgto);
      printf("Subject            : %s\n",msgsubj);
      printf("Control information:\n");
      fofs = ftell(fpkt);
      do
      {
         status = readasciicr(fpkt,linebuf,1024);
         if (linebuf[0]=='\1')
         {
            printf("^A");
            displayline(linebuf+1,2);
         }
         else if (memcmp(linebuf,"AREA:",5)==0
               || memcmp(linebuf,"SEEN-BY:",8)==0)
         {
            displayline(linebuf,0);
         }
      }
      while (status);
      printf("Message text:\n");
      fseek(fpkt,fofs,SEEK_SET);
      do
      {
         status = readasciicr(fpkt,linebuf,1024);
         if (linebuf[0]!='\1'
          && memcmp(linebuf,"AREA:",5)!=0
          && memcmp(linebuf,"SEEN-BY:",8)!=0)
         {
            displayline(linebuf,0);
         }
      }
      while (status);
   }

   fseek(fpkt,0,SEEK_END);
   printf("\nPacket contains %ld packed message(s) and is %ld bytes long.\n\n",
          msgcount, ftell(fpkt));

   fclose(fpkt);

   return 0;

abort:

   printf("ERROR: %s\n", error);

   if (fpkt!=NULL)
      fclose(fpkt);

   return 1;
}
/*...e*/

/*...smain:0:*/
int main(int argc, char *argv[])
{
   int i;

   printf("PKTView - Fidonet *.PKT viewer - last update 1999-07-27\n");
   printf("Written by Tom Torfs, free for all use\n\n");

   if (argc<2)
   {
      printf("This program can be used to view the contents of a Fidonet\n");
      printf("Type 2 or Type 2+ PKT mail packet file.\n\n");

      printf("Usage: pktview filespec [filespec ...]\n");
      
      return EXIT_FAILURE;
   }

   for (i=1; i<argc; i++)
   {
      pktview(argv[i]);
   }

   return EXIT_SUCCESS;
}
/*...e*/
